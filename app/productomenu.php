<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productomenu extends Model
{
    protected $table = 'productomenu';
    protected $primaryKey = 'idProductoMenu';
    public $timestamps = false;
}
