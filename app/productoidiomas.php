<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productoidiomas extends Model
{
    protected $table = 'productoidiomas';
    protected $primaryKey = 'idProductoIdioma';
    public $timestamps = false;
}
