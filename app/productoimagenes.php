<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productoimagenes extends Model
{
    protected $table = 'productoimagenes';
    protected $primaryKey = 'idImagen';
    public $timestamps = false;
}
