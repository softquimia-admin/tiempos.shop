<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class envios_cotizacion extends Model
{
    //
    protected $table = 'envios_cotizacion';
    protected $primaryKey = 'idcotizacion';
    public $timestamps = false;
}
