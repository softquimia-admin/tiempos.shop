<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productovariantesdetalle extends Model
{
    protected $table = 'productovariantesdetalle';
    protected $primaryKey = 'idProductoVarianteDetalle';
    public $timestamps = false;
}
