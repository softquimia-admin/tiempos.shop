<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class variantesdetalle extends Model
{
    protected $table = 'variantesdetalle';
    protected $primaryKey = 'idvariantedetalle';
    public $timestamps = false;
}
