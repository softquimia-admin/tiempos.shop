@extends('layout')

@section('content')

    <ventas></ventas>
    
@endsection

<style>
     table, th, td {
        border-bottom: 1px solid black;
        border-collapse: collapse;
    }
    
    th, td{
        padding: 10px;  
    }

    button{
    text-decoration: none;
    padding: 10px;
    font-weight: 300;
    font-size: 15px;
    color: #ffffff;
    background-color: #71bbbb;
    border-radius: 8px;
    border: 1px solid black;
  }
</style>